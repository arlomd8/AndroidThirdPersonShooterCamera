﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour
{
    public enum ThirdPersonCameraType
    {
        Track,
        Follow,
        Follow_TrackRotation,
        Follow_IndependentRotation,
        TopDown
    }

    public ThirdPersonCameraType tpCamType = ThirdPersonCameraType.Follow_IndependentRotation;
    public Transform player;
    public Vector3 positionOffset = new Vector3(0.0f, 2.0f, -2.5f);
    public Vector3 angleOffset = new Vector3(0.0f, 0.0f, 0.0f);

    [Tooltip("The damping factor to smooth the changes in position and rotation of the camera.")]
    public float damping = 1.0f;

    [Header("Follow Independent Rotation")]
    public float minPitch = -30.0f;
    public float maxPitch = 30.0f;
    public float rotationSpeed = 5.0f;
    private float angleX = 0.0f;

#if UNITY_ANDROID
    public FixedTouchField touchField;
#endif


    void Start()
    {

    }
    void Update()
    {
    }
    void LateUpdate()
    {
        switch (tpCamType)
        {
            case ThirdPersonCameraType.Track:
                {
                    CameraMove_Track();
                    break;
                }
            case ThirdPersonCameraType.Follow:
                {
                    CameraMove_Follow();
                    break;
                }
            case ThirdPersonCameraType.Follow_TrackRotation:
                {
                    CameraMove_Follow(true);
                    break;
                }
            case ThirdPersonCameraType.Follow_IndependentRotation:
                {
                    Follow_IndependentRotation();
                    break;
                }
            case ThirdPersonCameraType.TopDown:
                {
                    CameraMove_TopDown();
                    break;
                }
        }
    }

    void CameraMove_Track()
    {
        Vector3 targetPos = player.transform.position;
        targetPos.y += positionOffset.y;
        transform.LookAt(targetPos);
    }

    void CameraMove_Follow(bool allowRotationTracking = false)
    {
        Quaternion initialRotation = Quaternion.Euler(angleOffset);
        if (allowRotationTracking)
        {
            Quaternion rotation = Quaternion.Lerp(transform.rotation, player.rotation * initialRotation, Time.deltaTime * damping);
            transform.rotation = rotation;
        }
        else
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, initialRotation, damping * Time.deltaTime);
        }
       
        Vector3 forward = transform.rotation * Vector3.forward;
        Vector3 right = transform.rotation * Vector3.right;
        Vector3 up = transform.rotation * Vector3.up;
       
        Vector3 targetPos = player.position;
        Vector3 desiredPosition = targetPos + forward * positionOffset.z + right * positionOffset.x + up * positionOffset.y;
       
        Vector3 position = Vector3.Lerp(transform.position, desiredPosition, Time.deltaTime * damping);
        transform.position = position;
    }

    void Follow_IndependentRotation()
    {
#if UNITY_STANDALONE
        float mouseX, mouseY;
        mouseX = Input.GetAxis("Mouse X");
        mouseY = Input.GetAxis("Mouse Y");
#endif

#if UNITY_ANDROID
        float mouseX, mouseY;
        mouseX = touchField.TouchDist.x * Time.deltaTime;
        mouseY = touchField.TouchDist.y * Time.deltaTime;
#endif


        Quaternion initialRotation = Quaternion.Euler(angleOffset);
        Vector3 euler = transform.rotation.eulerAngles;
        angleX -= mouseY * rotationSpeed;

        angleX = Mathf.Clamp(angleX, minPitch, maxPitch);
        euler.y += mouseX * rotationSpeed;
        Quaternion newRotation = Quaternion.Euler(angleX, euler.y, 0.0f) * initialRotation;
        transform.rotation = newRotation;

        Vector3 forward = transform.rotation * Vector3.forward;
        Vector3 right = transform.rotation * Vector3.right;
        Vector3 up = transform.rotation * Vector3.up;

        Vector3 targetPos = player.position;
        Vector3 desiredPosition = targetPos + forward * positionOffset.z + right * positionOffset.x + up * positionOffset.y;
        Vector3 position = Vector3.Lerp(transform.position, desiredPosition, Time.deltaTime * damping);
        transform.position = position;
    }

    void CameraMove_TopDown()
    {
        Vector3 targetPos = player.position;
        targetPos.y += positionOffset.y;
        Vector3 position = Vector3.Lerp(transform.position, targetPos, Time.deltaTime * damping);
        transform.position = position;
        transform.rotation = Quaternion.Euler(90.0f, 0.0f, 0.0f);
    }
}